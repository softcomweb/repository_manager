export project="repository_manager"
export project_id="44641571"
export TF_VAR_gitlab_token=$(cat ~/.ssh/private/gitlab-leadel-kolo-token)
terraform init \
                -backend-config="address=https://gitlab.com/api/v4/projects/${project_id}/terraform/state/${project}" \
                -backend-config="lock_address=https://gitlab.com/api/v4/projects/${project_id}/terraform/state/${project}/lock" \
                -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${project_id}/terraform/state/${project}/lock" \
                -backend-config="username=${USER}" \
                -backend-config="password=$TF_VAR_gitlab_token" \
                -backend-config="lock_method=POST" \
                -backend-config="unlock_method=DELETE" \
                -backend-config="retry_wait_min=5" --reconfigure
