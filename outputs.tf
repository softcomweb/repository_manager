output "softcomweb_namespace" {
  value = data.gitlab_groups.softcomweb.groups[0].name
}

output "all_softcomweb_projects" {
  value = data.gitlab_projects.softcomweb_projects.projects[*].name
}
