## Welcome to my Repository manager project

To use this module you will need the gitlab_token exported as environment variable

```
export TF_VAR_gitlab_token=$(cat ~/.ssh/leadel_kolo_access_token)

```


# initialize terraform with http backend
```
    export project="repository_manager"
    export project_id="44641571"
	
    terraform init \
		-backend-config="address=https://gitlab.com/api/v4/projects/${project_id}/terraform/state/${project}" \
		-backend-config="lock_address=https://gitlab.com/api/v4/projects/${project_id}/terraform/state/${project}/lock" \
		-backend-config="unlock_address=https://gitlab.com/api/v4/projects/${project_id}/terraform/state/${project}/lock" \
		-backend-config="username=${USER}" \
		-backend-config="password=$TF_VAR_gitlab_token" \
		-backend-config="lock_method=POST" \
		-backend-config="unlock_method=DELETE" \
		-backend-config="retry_wait_min=5" --reconfigure
```
