format:
	@terraform fmt

validate: format
	@terraform validate

plan: validate
	@terraform plan

apply: plan
	@terraform apply -auto-approve

destroy:
	@terraform destroy -auto-approve