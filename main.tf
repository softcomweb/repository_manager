
locals {
  softcomweb_projects = {
    "dependabot" = {
      project_name = "softcomweb_dependabot",
      description  = "Softcomweb dependabot"
    }
    "renovate" = {
      project_name = "softcomweb_renovate",
      description  = "softcomweb renovate"
    }
    "azure" = {
      project_name = "softcomweb_azure",
      description  = "softcomweb azure project"
    }
    "testcontainers" = {
      project_name = "softcomweb_testcontainers",
      description  = "softcomweb testcontainers"
    }
  }
}

data "gitlab_groups" "softcomweb" {
  search = "softcomweb"
}

data "gitlab_projects" "softcomweb_projects" {
  group_id          = data.gitlab_groups.softcomweb.groups[0].group_id
  order_by          = "name"
  include_subgroups = true
  with_shared       = false
}

resource "gitlab_project" "gitlab_projects" {
  for_each         = local.softcomweb_projects
  name             = each.value.project_name
  description      = each.value.description
  visibility_level = "public"
  initialize_with_readme = true
  namespace_id     = data.gitlab_groups.softcomweb.groups[0].group_id
}
